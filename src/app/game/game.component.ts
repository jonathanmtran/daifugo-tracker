import { Component, OnInit } from '@angular/core';
import { CARDS } from '../mock-cards';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {

  cards;
  flips;

  constructor() {
    this.init();
  }

  ngOnInit() {
  }

  flip(): void {
    this.cards.reverse();
    this.flips++;
  }

  init(): void {
    this.cards = CARDS.slice(0);
    this.flips = 0;
  }

  reset(): void {
    this.init();
  }
}
