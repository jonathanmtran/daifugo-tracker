import { Card } from './card';

export const CARDS: Card[] = [
    { id: 1, name: 'King of Hearts'},
    { id: 2, name: 'One-eyed Jack'},
    { id: 3, name: '2'},
    { id: 4, name: 'Ace'},
    { id: 5, name: 'King'},
    { id: 6, name: 'Queen'},
    { id: 7, name: 'Jack'},
    { id: 8, name: '10'},
    { id: 9, name: '9'},
    { id: 10, name: '8'},
    { id: 11, name: '7'},
    { id: 12, name: '6'},
    { id: 13, name: '5'},
    { id: 14, name: '4'},
    { id: 15, name: '3'},
];
